#!/bin/bash

# Author : Bryan Haymoz
# Date   : 12.01.2022
# Version: 1.0
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: To recuperate ssh key create by script install_software for OpenStack

WHOAMI=$(whoami)
SSHKEY=$(cat /home/$WHOAMI/.ssh/id_rsa.pub)

jq -n --arg SSHKEY "$SSHKEY" '{"SSHKEY":$SSHKEY}'