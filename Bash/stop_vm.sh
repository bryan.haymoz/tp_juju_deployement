#!/bin/bash

# Author : Bryan Haymoz
# Date   : 31.01.2022
# Version: 1.1
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: To stop OpenStack Instance

# if empty parameter is passed, show parameters available
if [ $# -lt 1 ] ; then
   echo "Parameters Need : Available parameters are {MAAS, Juju, Node1, Node2 or Node3}  or {status, all}" 
   exit 1
fi;

# if status paremeter is passed, show all vm status
if [ $1 = "status" ]; then
    openstack server list  -c Name -c Status
    exit 1    

# if all parameter is passed, stop all vm
elif [ $1 = "all" ]; then
    status=$(echo -n "$(openstack server list -f value -c Status)" | tr '\n' ' ')

    # if one or more vm is already stop, exit
    if [[ "$status" == *"SHUTOFF"* ]]; then
        echo "can't stop all machine because one or more is already stop"
        vm_stop=$(echo -n "$(openstack server list -f value -c Name -c Status)" | grep "SHUTOFF" | tr '\n' ' ')
        echo $vm_stop
        exit 1
    else
    # stop all vm's
        all=$(echo -n "$(openstack server list -f value -c Name)" | tr '\n' ' ')
        openstack server stop $all
        while [[ "$status" == *"ACTIVE"* ]]
        do 
            status=$(echo -n "$(openstack server list -f value -c Status)" | tr '\n' ' ')
            echo "VM's are stopping ..."
            sleep 5
        done
        echo -e "\n"
        echo "VM's are stopped ..."
        exit 1
    fi;
else
    for vm in "$@" 
    do
        status_vm=$(echo -n "$(openstack server list -f value --name $vm -c Status)" | tr '\n' ' ')
        # if incorrect paramter show message
        if [[ $vm != "MAAS" ]] && [[ $vm != "Juju" ]] && [[ $vm != "Node1" ]] && [[ $vm != "Node2" ]] && [[ $vm != "Node3" ]]; then
           echo "Parameters Not Available : $vm"
           echo "Available parameters are {MAAS, Juju, Node1, Node2 or Node3}  or {status, all}" 
           continue
        # if VM is already shutoff
        elif [[ "$status_vm" == *"SHUTOFF"* ]]; then
           echo "can't stop VM $vm because it is stopped"
        else
            #stop VM's in order
            openstack server stop $vm
            while [ "$status_vm" == "ACTIVE" ]
            do 
                status_vm=$(echo -n "$(openstack server list -f value --name $vm -c Status)" | tr '\n' ' ')
                echo "VM $vm is stopping ..."
                sleep 5
            done
            echo "VM $vm is stopped"
            echo -e "\n"
        fi;
    done
fi;
