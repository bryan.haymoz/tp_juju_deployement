# Author : Bryan Haymoz
# Date   : 01.02.2022
# Version: 1.1
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: To create inventory of Terraform for Ansible

resource "local_file" "AnsibleInventory" {
content = templatefile("ansible_inventory.tpl", {
  hostname_MAAS        = "${openstack_compute_instance_v2.MAAS.name}",
  IP_MAAS     = "${openstack_networking_floatingip_v2.floating_IP_MAAS.address}",
  user_MAAS = "${var.ssh_user.MAAS}",

  hostname_Juju        = "${openstack_compute_instance_v2.Juju.name}",
  IP_Juju     = "${openstack_networking_floatingip_v2.floating_IP_Juju.address}",
  user_Juju = "${var.ssh_user.Juju}",

  hostname_Node1        = "${openstack_compute_instance_v2.Node1.name}",
  IP_Node1     = "${openstack_networking_floatingip_v2.floating_IP_Node1.address}",
  user_Node1 = "${var.ssh_user.Node1}",

  hostname_Node2        = "${openstack_compute_instance_v2.Node2.name}",
  IP_Node2     = "${openstack_networking_floatingip_v2.floating_IP_Node2.address}",
  user_Node2 = "${var.ssh_user.Node2}",


  hostname_Node3        = "${openstack_compute_instance_v2.Node3.name}",
  IP_Node3     = "${openstack_networking_floatingip_v2.floating_IP_Node3.address}",
  user_Node3 = "${var.ssh_user.Node3}",

  port_SSH = "${var.port_SSH}",
 }
 )
 filename = "../Ansible/dynamic_inventory"
}

resource "local_file" "MAAS" {
content = templatefile("MAAS_inventory.tpl", {
  IP_MAAS     = "${openstack_networking_floatingip_v2.floating_IP_MAAS.address}",
 }
 )
 filename = "../Ansible/IP_MAAS"
}
