# Author : Bryan Haymoz
# Date   : 12.01.2022
# Version: 1.1
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: Variables description and default value

#below variable values are secret and kept in tfvars
variable "openstack_user_name"  { description = "Openstack user name" }
variable "openstack_tenant_name"    { description = "Openstack tenant name" }
variable "openstack_password"   { description = "Openstack password" }
variable "openstack_auth_url"   { description = "Openstack auth url" }
variable "openstack_region" { description = "Openstack region" }

#network variables
variable "service_network_cidr" { default = "192.168.11.0/24" }
variable "service_ip_gateway"   { default = "192.168.11.1" }
variable "service_subnet_start_pool"    {default = "192.168.11.10"}
variable "service_subnet_end_pool"  {default = "192.168.11.110"}
variable "dns_server_1" { default = "8.8.8.8" }
variable "dns_server_2" { default = "8.8.4.4" }

variable "pxe_network_cidr" { default = "192.168.12.0/24" }

#instance variables
variable "flavor_MAAS" { default = "c1.medium" }
variable "image_linux" { default = "Ubuntu Focal 20.04 (SWITCHengines)" }
variable "flavor_Juju" { default = "c1.large" }
variable "flavor_Node" { default = "c1.medium" }
variable "image_pxe" { default = "pxe_image" }
variable "image_pxe_source" { default = "http://cern.ch/linux/centos7/docs/pxeboot.img"}

variable "IP_Service_MAAS" {default = "192.168.11.10"}
variable "IP_PXE_MAAS" {default = "192.168.12.1"}

variable "IP_Service_Juju" {default = "192.168.11.11"}
variable "IP_PXE_Juju" {default = "192.168.12.11"}

variable "IP_Service_Node1" {default = "192.168.11.12"}
variable "IP_PXE_Node1" {default = "192.168.12.12"}

variable "IP_Service_Node2" {default = "192.168.11.13"}
variable "IP_PXE_Node2" {default = "192.168.12.13"}

variable "IP_Service_Node3" {default = "192.168.11.14"}
variable "IP_PXE_Node3" {default = "192.168.12.14"}

variable volume_size_node {default = 20}


#for dynamic inventory
variable "port_SSH" { default = "22" }

variable "ssh_user" {
  type = map(string)
  default = {
    "MAAS" = "ubuntu"
    "Juju"  = "ubuntu"
    "Node1" = "ubuntu"
    "Node2"  = "ubuntu"
    "Node3" = "ubuntu"
  }
}