# Author : Bryan Haymoz
# Date   : 08.12.2021
# Version: 1.0
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: To connect to API Provider

#Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.35.0"
    }
  }
}

# Configure OpenStack provider
provider "openstack" {
  user_name   = "${var.openstack_user_name}"
  tenant_name = "${var.openstack_tenant_name}"
  password    = "${var.openstack_password}"
  auth_url    = "${var.openstack_auth_url}"
  region      = "${var.openstack_region}"
}


