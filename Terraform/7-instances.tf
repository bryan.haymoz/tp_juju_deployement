# Author : Bryan Haymoz
# Date   : 30.01.2022
# Version: 1.3
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: To deploy instance

##################################################   MAAS   ################################################## 
# Create network port to service network
resource "openstack_networking_port_v2" "MAAS_port_service" {
  name = "MAAS_port_service"
  network_id = "${openstack_networking_network_v2.service_network.id}"
  admin_state_up = "true"
  port_security_enabled = "true"
  security_group_ids = ["${openstack_compute_secgroup_v2.secgroup_1.id}"]
   fixed_ip {
    subnet_id = "${openstack_networking_subnet_v2.service_subnet.id}"
    ip_address = var.IP_Service_MAAS
  }
}

# Create network port to pxe network
resource "openstack_networking_port_v2" "MAAS_port_pxe" {
  name = "MAAS_port_pxe"
  network_id = "${openstack_networking_network_v2.pxe_network.id}"
  admin_state_up = "true"
  port_security_enabled = "false"
  no_security_groups = "true"
  fixed_ip {
    subnet_id = "${openstack_networking_subnet_v2.pxe_subnet.id}"
    ip_address = var.IP_PXE_MAAS
  }
}

#Create instance
resource "openstack_compute_instance_v2" "MAAS" {
  name = "MAAS"
  image_name =  var.image_linux
  flavor_name = var.flavor_MAAS
  key_pair    = openstack_compute_keypair_v2.vm-key.name
  stop_before_destroy = "true"
  network {
    name = "service_network"
    port = "${openstack_networking_port_v2.MAAS_port_service.id}"
  }
  network {
    name = "pxe_network"
    port = "${openstack_networking_port_v2.MAAS_port_pxe.id}"
  }
}


#############################################################################################################################################################
#############################################################################################################################################################

##################################################   Juju   ################################################## 

# Create network port to pxe network
resource "openstack_networking_port_v2" "Juju_port_pxe" {
  name = "Juju_port_pxe"
  network_id = "${openstack_networking_network_v2.pxe_network.id}"
  admin_state_up = "true"
  port_security_enabled = "false"
  no_security_groups = "true"
  fixed_ip {
    subnet_id = "${openstack_networking_subnet_v2.pxe_subnet.id}"
    ip_address = var.IP_PXE_Juju
  }
}

# Create network port to service network
resource "openstack_networking_port_v2" "Juju_port_service" {
  name = "Juju_port_service"
  network_id = "${openstack_networking_network_v2.service_network.id}"
  admin_state_up = "true"
  port_security_enabled = "true"
  security_group_ids = ["${openstack_compute_secgroup_v2.secgroup_1.id}"]
   fixed_ip {
    subnet_id = "${openstack_networking_subnet_v2.service_subnet.id}"
    ip_address = var.IP_Service_Juju
  }
}

#Create instance
resource "openstack_compute_instance_v2" "Juju" {
  name = "Juju"
  image_name =  var.image_pxe
  flavor_name = var.flavor_Juju
  power_state = "shutoff"
  key_pair    = openstack_compute_keypair_v2.vm-key.name
  stop_before_destroy = "true"
  network {
    name = "pxe_network"
    port = "${openstack_networking_port_v2.Juju_port_pxe.id}"
  }
  network {
    name = "service_network"
    port = "${openstack_networking_port_v2.Juju_port_service.id}"
  }
}

#############################################################################################################################################################
#############################################################################################################################################################

##################################################   Node1   ################################################## 

# Create network port to pxe network
resource "openstack_networking_port_v2" "Node1_port_pxe" {
  name = "Node1_port_pxe"
  network_id = "${openstack_networking_network_v2.pxe_network.id}"
  admin_state_up = "true"
  port_security_enabled = "false"
  no_security_groups = "true"
  fixed_ip {
    subnet_id = "${openstack_networking_subnet_v2.pxe_subnet.id}"
    ip_address = var.IP_PXE_Node1
  }
}

# Create network port to service network
resource "openstack_networking_port_v2" "Node1_port_service" {
  name = "Node1_port_service"
  network_id = "${openstack_networking_network_v2.service_network.id}"
  admin_state_up = "true"
  port_security_enabled = "true"
  security_group_ids = ["${openstack_compute_secgroup_v2.secgroup_1.id}"]
   fixed_ip {
    subnet_id = "${openstack_networking_subnet_v2.service_subnet.id}"
    ip_address = var.IP_Service_Node1
  }
}

#Create instance
resource "openstack_compute_instance_v2" "Node1" {
  name = "Node1"
  image_name =  var.image_pxe
  flavor_name = var.flavor_Node
  power_state = "shutoff"
  key_pair    = openstack_compute_keypair_v2.vm-key.name
  stop_before_destroy = "true"
  network {
    name = "pxe_network"
    port = "${openstack_networking_port_v2.Node1_port_pxe.id}"
  }
  network {
    name = "service_network"
    port = "${openstack_networking_port_v2.Node1_port_service.id}"
  }
}



#Create a volume
#Size volume in Gio
resource "openstack_blockstorage_volume_v2" "volume_Node1" {
  name = "volume_Node1"
  size = var.volume_size_node
}

#Associate the volume to instance
resource "openstack_compute_volume_attach_v2" "va_Node1" {
  instance_id = "${openstack_compute_instance_v2.Node1.id}"
  volume_id   = "${openstack_blockstorage_volume_v2.volume_Node1.id}"
}

#############################################################################################################################################################
#############################################################################################################################################################

##################################################   Node2   ################################################## 

# Create network port to pxe network
resource "openstack_networking_port_v2" "Node2_port_pxe" {
  name = "Node2_port_pxe"
  network_id = "${openstack_networking_network_v2.pxe_network.id}"
  admin_state_up = "true"
  port_security_enabled = "false"
  no_security_groups = "true"
  fixed_ip {
    subnet_id = "${openstack_networking_subnet_v2.pxe_subnet.id}"
    ip_address = var.IP_PXE_Node2
  }
}

# Create network port to service network
resource "openstack_networking_port_v2" "Node2_port_service" {
  name = "Node2_port_service"
  network_id = "${openstack_networking_network_v2.service_network.id}"
  admin_state_up = "true"
  port_security_enabled = "true"
  security_group_ids = ["${openstack_compute_secgroup_v2.secgroup_1.id}"]
   fixed_ip {
    subnet_id = "${openstack_networking_subnet_v2.service_subnet.id}"
    ip_address = var.IP_Service_Node2
  }
}

#Create instance
resource "openstack_compute_instance_v2" "Node2" {
  name = "Node2"
  image_name =  var.image_pxe
  flavor_name = var.flavor_Node
  power_state = "shutoff"
  key_pair    = openstack_compute_keypair_v2.vm-key.name
  stop_before_destroy = "true"
  network {
    name = "pxe_network"
    port = "${openstack_networking_port_v2.Node2_port_pxe.id}"
  }

  network {
    name = "service_network"
    port = "${openstack_networking_port_v2.Node2_port_service.id}"
  }
}


#Create a volume
#Size volume in Gio
resource "openstack_blockstorage_volume_v2" "volume_Node2" {
  name = "volume_Node2"
  size = var.volume_size_node
}

#Associate the volume to instance
resource "openstack_compute_volume_attach_v2" "va_Node2" {
  instance_id = "${openstack_compute_instance_v2.Node2.id}"
  volume_id   = "${openstack_blockstorage_volume_v2.volume_Node2.id}"
}

#############################################################################################################################################################
#############################################################################################################################################################

##################################################   Node3   ################################################## 
# Create network port to pxe network
resource "openstack_networking_port_v2" "Node3_port_pxe" {
  name = "Node3_port_pxe"
  network_id = "${openstack_networking_network_v2.pxe_network.id}"
  admin_state_up = "true"
  port_security_enabled = "false"
  no_security_groups = "true"
  fixed_ip {
    subnet_id = "${openstack_networking_subnet_v2.pxe_subnet.id}"
    ip_address = var.IP_PXE_Node3
  }
}

# Create network port to service network
resource "openstack_networking_port_v2" "Node3_port_service" {
  name = "Node3_port_service"
  network_id = "${openstack_networking_network_v2.service_network.id}"
  admin_state_up = "true"
  port_security_enabled = "true"
  security_group_ids = ["${openstack_compute_secgroup_v2.secgroup_1.id}"]
   fixed_ip {
    subnet_id = "${openstack_networking_subnet_v2.service_subnet.id}"
    ip_address = var.IP_Service_Node3
  }
}

#Create instance
resource "openstack_compute_instance_v2" "Node3" {
  name = "Node3"
  image_name =  var.image_pxe
  flavor_name = var.flavor_Node
  power_state = "shutoff"
  key_pair    = openstack_compute_keypair_v2.vm-key.name
    stop_before_destroy = "true"

  network {
    name = "pxe_network"
    port = "${openstack_networking_port_v2.Node3_port_pxe.id}"
  }
  network {
    name = "service_network"
    port = "${openstack_networking_port_v2.Node3_port_service.id}"
  }
}

#Create a volume
#Size volume in Gio
resource "openstack_blockstorage_volume_v2" "volume_Node3" {
  name = "volume_Node3"
  size = var.volume_size_node
}

#Associate the volume to instance
resource "openstack_compute_volume_attach_v2" "va_Node3" {
  instance_id = "${openstack_compute_instance_v2.Node3.id}"
  volume_id   = "${openstack_blockstorage_volume_v2.volume_Node3.id}"
}
